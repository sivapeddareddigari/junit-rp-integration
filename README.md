# Junit5 - Maven - Reportportal Integration

The `Junit5 Test results integration with ReportPortal` project demonstrates how to execute & push the JUnit5 Test cases and results to Reportportal.io using Maven.

While TestNG is being used extensively by the QE fraternity especially in System, System Integration and Regression Testing, Junit5 is its nearest cousin for
unit testing with a ton of powerful features than its predecessors.

Nevertheless, Junit 5 still does not have all the cool features of TestNG like Scenario Testing, Parallel Execution etc.,.

Recently, Reportportal.io has gained immance popularity among key project stakeholders as it enables and empowers developers and QE practitioners to collate, consolidate, manage and view all of their
Test execution results, defect classification, defect management by integrating to the popular Defect Management tools( ....and host of other features) - all in one place
You can quickly refer the brochure [here](https://www.epam.com/content/dam/epam/free_library/Brochures/Report_Portal_Brochure.pdf)

This sample project provides a quick template with all the required components assembled and sample test cases included
Please follow the steps below to integrate your project unit test results to Reportportal.io 
####Pre-requisite
You should have a running reportportal
Review the instructions - easiest way is to spinning up Reportportal docker container(s)

##Step 1
###Reportportal.io configuration
1. Login to ReportPortal.io with a username/password
![img.png](img.png)
2. Navigate to Profile
3. ![img_1.png](img_1.png)
4. Select JAVA and copy the config parameters
![img_2.png](img_2.png)
5. Add a file "reportportal.properties" under src/main/resources folder and paste the copied configuration
![img_3.png](img_3.png)

###POM file configuration
1. Review the https://reportportal.io/installation page to understand the available integrations and required configurations
![img_4.png](img_4.png)
2. From Junit5 perspective review  https://github.com/reportportal/agent-java-junit5
and follow the steps.
3. Add the required dependencies and plugins as indicated.
Alternatively you can add refer to the POM.xml file of this project

###Write the JUNIT Test cases
Refer to https://junit.org/junit5/docs/current/user-guide/#writing-tests

###Execute the test cases
use maven command with the goals:  mvn clean test 
While you run this command, the plugins (embedded configuration in POM file) does the magic of creating xml reports reading the Reportportal properties from its configuration file
and connecting and persisting the reports on to Reportportal.

NOTE: Currently Intellij IDE supports Junit5. However, the tests executed using IDE will never reach ReportPortal even though with the right configuration,
primarily because such execution will not invoke required maven plugins. Hence use maven command line : mvn clean test

#Happy Reporting!