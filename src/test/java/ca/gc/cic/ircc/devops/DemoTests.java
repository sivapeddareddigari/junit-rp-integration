/*
 * Copyright 2015-2021 the original author or authors.
 *
 * All rights reserved. This program and the accompanying materials are
 * made available under the terms of the Eclipse Public License v2.0 which
 * accompanies this distribution and is available at
 *
 * http://www.eclipse.org/legal/epl-v20.html
 */

package ca.gc.cic.ircc.devops;

import static org.junit.jupiter.api.Assertions.assertEquals;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.MethodOrderer;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;
import com.epam.reportportal.junit5.ReportPortalExtension;
import org.junit.jupiter.api.TestMethodOrder;
import org.junit.jupiter.api.extension.ExtendWith;
@ExtendWith(ReportPortalExtension.class)
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class DemoTests {
    private static final Logger LOGGER = LogManager.getLogger(DemoTests.class);
    @Test
    @Order(1)
    void AssertequalTest() {

        assertEquals(2, 2, "1 + 1 should equal 2");
        LOGGER.info("Test Case execution Order 1");
    }
    @Test
    @Order(2)
    void AssertfailTest() {

        assertEquals(10, 10, "1 + 1 should equal 2");
        LOGGER.info("Test Case execution Order 2");
    }

}
